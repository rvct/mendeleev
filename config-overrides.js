/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react-hooks/rules-of-hooks */
const {
  override,
  useBabelRc,
  addWebpackAlias,
  setWebpackOptimizationSplitChunks,
} = require("customize-cra");
const fs = require("fs");

const dotenvFiles = [".env", ".env.local"];

let extendedEnv = {};

dotenvFiles.forEach((file) => {
  if (fs.existsSync(file)) {
    let { parsed } = require("dotenv").config({
      path: file,
    });

    Object.entries(parsed).forEach(([key, value]) => {
      extendedEnv[key] = JSON.stringify(value);
    });
  }
});

const findWebpackPlugin = (plugins, pluginName) =>
  plugins.find((plugin) => plugin.constructor.name === pluginName);

const overrideProcessEnv = (value) => (config) => {
  const plugin = findWebpackPlugin(config.plugins, "DefinePlugin");
  const processEnv = plugin.definitions["process.env"] || {};

  plugin.definitions["process.env"] = {
    ...processEnv,
    ...value,
  };

  return config;
};

const createCacheGroup = (groups) => {
  const includeDepsToChunk = (deps) => `(${deps.join("|")})`;
  const excludeDepsFromChunk = (deps) =>
    deps.reduce((acc, item) => acc.concat(`(!${item})`), "");

  const deps = Object.values(groups).flat();

  const cacheGroups = Object.entries(groups).reduce(
    (acc, [group, groupDeps]) => ({
      ...acc,
      [group]: {
        test: new RegExp(
          `[\\/]node_modules[\\/]${includeDepsToChunk(groupDeps)}[\\/]`
        ),
        name: group,
      },
    }),
    {
      vendor: {
        test: new RegExp(
          `[\\/]node_modules[\\/]${excludeDepsFromChunk(deps)}[\\/]`
        ),
        name: "vendor",
      },
    }
  );

  console.warn("chunks\n\n", cacheGroups);

  return cacheGroups;
};

module.exports = {
  webpack: override(
    useBabelRc(),
    overrideProcessEnv(extendedEnv),
    addWebpackAlias({
      react: "preact/compat",
      "react-dom/test-utils": "preact/test-utils",
      "react-dom": "preact/compat",
    }),
    setWebpackOptimizationSplitChunks({
      chunks: "all",
      maxInitialRequests: Infinity,
      cacheGroups: createCacheGroup({
        highcharts: ["highcharts", "highcharts-react-official"],
      }),
    })
  ),
};
